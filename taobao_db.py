#!/usr/bin/env python
#coding:utf-8
from min_p import *
reload(sys)
sys.setdefaultencoding("utf-8")
import sqlite3

class TB_DB():
    def __init__(self):
        #如果没有数据库就新建数据库
        if not os.path.exists("TB.db"):
            self.conn = sqlite3.connect('TB.db')
            self.cursor = self.conn.cursor()
            self.cursor.execute('''CREATE TABLE TB_ORDER (
                                    id integer PRIMARY KEY autoincrement,
                                    order_id  varchar(30) NOT NULL,
                                    order_creat_time  varchar(20) NOT NULL,
                                    buyer_name  varchar(20) NOT NULL,
                                    order_state  varchar(20) NOT NULL,
                                    outer_iid  varchar(20),
                                    refund  varchar(20),
                                    order_json varchar(20))
                                ''')
            self.add("0000000000","2000-01-01 00:00:00","000000000","","","","")
            self.cursor.close()
            self.conn.commit()
            self.conn.close()

        self.conn = sqlite3.connect('TB.db')
        self.cursor = self.conn.cursor()

    #插入数据(订单编号，订单创建时间，用户id，订单状态，商家编码，退款状态)
    def add(self,order_id,order_creat_time,buyer_name,order_state,outer_iid,refund):
        try:
            self.cursor.execute('insert into TB_ORDER (order_id,order_creat_time,buyer_name,order_state,outer_iid,refund) values (?,?,?,?,?,?)', (order_id,order_creat_time,buyer_name,order_state,outer_iid,refund))
            self.conn.commit()
        except Exception, e:
            print e
            return log_wrong()

    #更新数据（订单编号，更新字段名称，更新字段内容)
    def update(self,order_id,update_name,update_value):
        try:
            self.cursor.execute("update TB_ORDER set %s =? where order_id=?"%update_name,(update_value,order_id))
            self.conn.commit()
            return True
        except Exception, e:
            print e
            return log_wrong()

    #查询(订单编号，查询字段)
    def search(self,search_name,order_id):
        try:
            self.cursor.execute('select `%s` from TB_ORDER where order_id=?'%search_name,(order_id,))
            return self.cursor.fetchall()[0][0]
        except Exception, e:
            #log_wrong()
            #print e
            return []
    #查询单条记录
    def search_one(self,order_id):
        try:
            self.cursor.execute('select * from TB_ORDER where order_id=?',(order_id,))
            return self.cursor.fetchall()[0]
        except Exception, e:
            #log_wrong()
            #print e
            return []
    #获取最后一条记录
    def get_last(self):
        self.cursor.execute('select * from TB_ORDER order by id desc LIMIT 1')
        return self.cursor.fetchall()[0]


    #数据库关闭
    def __del__(self):
        self.cursor.close()
        self.conn.close()


if __name__ == '__main__':
    new_tb = TB_DB()
    print new_tb.get_last()
    print new_tb.search("order_id","0000000000")

